package demo.bachelorthesis.controller;

import demo.bachelorthesis.patterns.behavioural.observer.BarChartObserver;
import demo.bachelorthesis.patterns.behavioural.observer.Observer;
import demo.bachelorthesis.patterns.behavioural.observer.PieChartObserver;
import demo.bachelorthesis.patterns.creational.prototype.SpringPrototype;
import demo.bachelorthesis.patterns.structural.decorator.*;
import demo.bachelorthesis.patterns.structural.proxy.Graphic;
import demo.bachelorthesis.patterns.structural.proxy.Image;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.web.context.support.GenericWebApplicationContext;

@Configuration
@ComponentScan("demo.bachelorthesis.controller")
public class WebControllerConfiguration {

    @Bean
    public GenericWebApplicationContext context() {
        return new GenericWebApplicationContext();
    }

    @Bean
    @Qualifier("observer")
    public Observer barChartObserver() {
        return new BarChartObserver();
    }

    @Bean
    @Qualifier("observer")
    public Observer pieChartObserver() {
        return new PieChartObserver();
    }

    @Bean
    public ProxyFactory proxyFactory() {
        ProxyFactory proxyFactory = new ProxyFactory(new Image());
        proxyFactory.addInterface(Graphic.class);
        proxyFactory.setExposeProxy(true);
        return proxyFactory;
    }

    @Bean
    @Scope("prototype")
    public SpringPrototype springPrototype() {
        return new SpringPrototype();
    }

    @Bean
    @Qualifier("notDecorated")
    public VisualComponent visualComponent() {
        return new TextView();
    }

    @Bean
    @Qualifier("border")
    public BorderDecorator borderDecorator(VisualComponent visualComponent) {
        return new BorderDecorator(visualComponent);
    }

    @Bean
    @Qualifier("scrollBar")
    public ScrollDecorator scrollDecorator(VisualComponent visualComponent) {
        return new ScrollDecorator(visualComponent);
    }

    @Bean
    @Qualifier("borderAndScrollBar")
    public ComponentDecorator borderAndScrollBarDecorator(VisualComponent visualComponent) {
        return new BorderDecorator(new ScrollDecorator(visualComponent));
    }

}
