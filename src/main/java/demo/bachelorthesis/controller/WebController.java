package demo.bachelorthesis.controller;

import demo.bachelorthesis.patterns.behavioural.command.CopyCommand;
import demo.bachelorthesis.patterns.behavioural.command.Document;
import demo.bachelorthesis.patterns.behavioural.command.PasteCommand;
import demo.bachelorthesis.patterns.behavioural.interpreter.StringCalculator;
import demo.bachelorthesis.patterns.behavioural.mediator.Font;
import demo.bachelorthesis.patterns.behavioural.mediator.FontDialogMediator;
import demo.bachelorthesis.patterns.behavioural.observer.CalculationResult;
import demo.bachelorthesis.patterns.behavioural.observer.Observer;
import demo.bachelorthesis.patterns.behavioural.template.InteractiveView;
import demo.bachelorthesis.patterns.behavioural.template.ResizableView;
import demo.bachelorthesis.patterns.creational.builder.Customer;
import demo.bachelorthesis.patterns.creational.builder.CustomerOccupation;
import demo.bachelorthesis.patterns.creational.factory.ControlElement;
import demo.bachelorthesis.patterns.creational.factory.ControlElementsFactory;
import demo.bachelorthesis.patterns.creational.prototype.SpringPrototype;
import demo.bachelorthesis.patterns.creational.singleton.SpringSingleton;
import demo.bachelorthesis.patterns.creational.singleton.TraditionalSingleton;
import demo.bachelorthesis.patterns.other.repository.CustomerEntity;
import demo.bachelorthesis.patterns.other.repository.CustomerRepository;
import demo.bachelorthesis.patterns.structural.adapter.TextShapeAdapter;
import demo.bachelorthesis.patterns.structural.adapter.TextBox;
import demo.bachelorthesis.patterns.structural.decorator.*;
import demo.bachelorthesis.patterns.structural.proxy.Graphic;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.support.GenericWebApplicationContext;

import java.util.List;

@RequestMapping("/")
@RestController
@Slf4j
public class WebController {

    private final ControlElementsFactory factory;

    private final SpringSingleton springSingleton1;
    private final SpringSingleton springSingleton2;

    private final SpringPrototype springPrototype1;
    private final SpringPrototype springPrototype2;

    private final TextShapeAdapter adapter;

    private final VisualComponent notDecoratedComponent;
    private final ScrollDecorator scrollDecorator;
    private final BorderDecorator borderDecorator;
    private final ComponentDecorator borderAndScrollDecorator;

    private final ProxyFactory proxyFactory;

    private final CustomerRepository repository;

    private final InteractiveView interactiveView;
    private final ResizableView resizableView;

    private final CalculationResult calculationResult;
    private final List<Observer> calculationResultObservers;

    private final CopyCommand copyCommand;
    private final PasteCommand pasteCommand;
    private final Document document;

    private final FontDialogMediator mediator;

    private final GenericWebApplicationContext context;

    public WebController (ControlElementsFactory factory,
                          SpringSingleton springSingleton1, SpringSingleton springSingleton2,
                          SpringPrototype springPrototype1, SpringPrototype springPrototype2,
                          TextShapeAdapter adapter,
                          @Qualifier("notDecorated") VisualComponent notDecoratedComponent,
                          @Qualifier("scrollBar") ScrollDecorator scrollDecorator,
                          @Qualifier("border") BorderDecorator borderDecorator,
                          @Qualifier("borderAndScrollBar") ComponentDecorator borderAndScrollDecorator,
                          ProxyFactory proxyFactory,
                          CustomerRepository repository,
                          @Qualifier("interact") InteractiveView interactiveView,
                          @Qualifier("resize") ResizableView resizableView,
                          @Qualifier("observer") List<Observer> calculationResultObservers,
                          CalculationResult calculationResult,
                          CopyCommand copyCommand,
                          PasteCommand pasteCommand,
                          Document document,
                          FontDialogMediator mediator,
                          GenericWebApplicationContext context) {
        this.factory = factory;
        this.springSingleton1 = springSingleton1;
        this.springSingleton2 = springSingleton2;
        this.springPrototype1 = springPrototype1;
        this.springPrototype2 = springPrototype2;
        this.adapter = adapter;
        this.notDecoratedComponent = notDecoratedComponent;
        this.scrollDecorator = scrollDecorator;
        this.borderDecorator = borderDecorator;
        this.borderAndScrollDecorator = borderAndScrollDecorator;
        this.proxyFactory = proxyFactory;
        this.repository = repository;
        this.interactiveView = interactiveView;
        this.resizableView = resizableView;
        this.calculationResultObservers = calculationResultObservers;
        this.calculationResult = calculationResult;
        this.copyCommand = copyCommand;
        this.pasteCommand = pasteCommand;
        this.document = document;
        this.mediator = mediator;
        this.context = context;
    }

    @PostMapping("factory/controlElement/type={controlElementType}/opacity={controlElementOpacity}")
    public ControlElement createControlElement(@PathVariable String controlElementType,
                                               @PathVariable int controlElementOpacity) {
         ControlElement controlElement = factory.createElement(controlElementType);
         controlElement.makeVisible(controlElementOpacity);
         return controlElement;
    }

    @PostMapping("builder")
    public Customer createCustomer(@RequestBody CustomerOccupation occupation) {
        return Customer.builder()
                .firstName(occupation.getFirstName())
                .lastName(occupation.getLastName())
                .employed(occupation.isEmployed())
                .company(occupation.getCompany())
                .occupation(occupation.getOccupation())
                .build();
    }

    @GetMapping("singleton")
    public void createSingletons() {
        TraditionalSingleton traditionalSingleton1 = TraditionalSingleton.getInstance();
        TraditionalSingleton traditionalSingleton2 = TraditionalSingleton.getInstance();
        try {
            Assert.isTrue(traditionalSingleton1 == traditionalSingleton2,
                          "Traditional singletons don't point to same instance.");
            Assert.isTrue(springSingleton1 == springSingleton2,
                          "Spring singletons don't point to same instance.");
        } catch (IllegalArgumentException ex) {
            log.info(ex.getMessage());
        }
        log.info("Traditional and Spring singletons are created as expected.");
    }

    @GetMapping("prototype")
    public void createPrototypes() {
        try {
            Assert.isTrue(springPrototype1 != springPrototype2,
                          "Spring prototypes point to same instance.");
        } catch (IllegalArgumentException ex) {
            log.info(ex.getMessage());
        }
        log.info("Spring prototypes are created as expected.");
    }

    @PostMapping("adapter/textShape/x={originX}/y={originY}/width={width}/height={height}")
    public String composeWithAdapter(@PathVariable int originX,
                                     @PathVariable int originY,
                                     @PathVariable int width,
                                     @PathVariable int height) {
        TextBox textBox = this.adapter;
        textBox.setOrigin(originX, originY);
        textBox.setExtent(width, height);

        return textBox.display();
    }

    @PostMapping("decorator/textView/withBorder={withBorder}/withScrollBar={withScrollBar}")
    public String composeWithDecorator(@PathVariable boolean withBorder,
                                       @PathVariable boolean withScrollBar){
        return withBorder && withScrollBar ?
                borderAndScrollDecorator.draw()
                : withBorder ? borderDecorator.draw()
                : withScrollBar ? scrollDecorator.draw()
                : notDecoratedComponent.draw();
    }

    @GetMapping("proxy")
    public void composeWithProxy() {
        Graphic graphic = (Graphic) proxyFactory.getProxy();
        graphic.store("LogoIcon");

        String className = graphic.getClass().getSimpleName();

        log.info("Loading status : {}", graphic.loadByName("LogoIcon"));
        log.info("Proxy status : Drawing object is proxied = {}. Is entity of a class {}",
                className.contains("Proxy"),
                className);
    }

    @GetMapping("repository/findCustomer/lastName={lastName}")
    public CustomerEntity getFromRepository(@PathVariable String lastName) {
        return repository.findByLastName(lastName);
    }

    @GetMapping("template/displayView/functionality={functionality}")
    public void behaveAsTemplate(@PathVariable String functionality) {
        log.info("================================================");
        switch (functionality) {
            case "interact" : interactiveView.doDisplay(); break;
            case "resize" : resizableView.doDisplay(); break;
            default: log.info("Only '{}' and '{}' functionality is supported for the view",
                    "interact", "resize");
        }
    }

    @GetMapping("observer/countRemainder/divider={divider}/divisor={divisor}")
    public void behaveAsObserver(@PathVariable int divider,
                                 @PathVariable int divisor) {
        calculationResult.calculateValueA(divider, divisor);
        log.info("Initial observers count is {}",
                calculationResult.countObservers());

        calculationResult.deleteObserver(calculationResultObservers.get(0));
        log.info("After observer removal observers count is {}",
                calculationResult.countObservers());
    }

    @PostMapping("command/copyPaste/text={text}")
    public void behaveAsCommand(@PathVariable String text) {
        copyCommand.execute(text);
        pasteCommand.execute(document.getCopiedText());
        log.info("Copy & Paste commands performed for '{}'",
                document.getPastedText());
    }

    @PostMapping("mediator/enter/text={text}")
    public void behaveAsMediator1(@PathVariable String text) {
        log.info("================================================");
        mediator.enterText(text);
    }

    @PostMapping("mediator/apply/font={font}")
    public void behaveAsMediator2(@PathVariable String font) {
        log.info("================================================");
        if (Font.isValid(font)) {
            mediator.selectFont(Font.enumOf(font));
        } else {
            log.info("Only {}, {} and {} fonts are supported for the text",
                    "BOLD", "ITALIC", "UNDERLINED");
        }
    }

    @GetMapping("interpreter/number1={number1}/number2={number2}")
    public void behaveAsInterpreter(@PathVariable String number1,
                                    @PathVariable String number2) {
        context.registerBean(
                "calculator",
                StringCalculator.class,
                () -> new StringCalculator(number1, number2));
        StringCalculator stringCalculator = (StringCalculator) context.getBean("calculator");
        log.info("{} plus {} equals {}", number1, number2, stringCalculator.getSum());
        log.info("{} subtract {} equals {}", number1, number2, stringCalculator.getSubtraction());
        log.info("{} multiply {} equals {}", number1, number2, stringCalculator.getMultiplication());
        log.info("{} divide by {} equals {}", number1, number2, stringCalculator.getDivision());
        context.removeBeanDefinition("calculator");
    }

}

