package demo.bachelorthesis.patterns.structural.adapter;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@NoArgsConstructor
@Component
public class ShapeElement implements Shape {

    private int width;
    private int height;
    private int originX;
    private int originY;
    private String shapeInfo;

    @Override
    public String display() {
        this.boundingBox();
        return this.shapeInfo;
    }

    @Override
    public void boundingBox() {
        this.shapeInfo = String.format(
                "Left bottom coordinates : x = %s, y = %s; " +
                "Right top coordinates : x = %s, y = %s",
                originX, originY, originX + width, originY + height);
    }

    @Override
    public void setOrigin(int x, int y) {
        this.originX = x;
        this.originY = y;
    }

    @Override
    public void setExtent(int width, int height) {
        this.width = width;
        this.height = height;
    }

}
