package demo.bachelorthesis.patterns.structural.adapter;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class TextShapeAdapter implements TextBox {

    private final Shape shape;

    @Override
    public void setOrigin(int x, int y) {
        this.shape.setOrigin(x, y);
    }

    @Override
    public void setExtent(int width, int height) {
        this.shape.setExtent(width, height);
    }

    @Override
    public String display() {
        this.shape.boundingBox();
        return this.shape.display();
    }

}
