package demo.bachelorthesis.patterns.structural.adapter;

public interface TextBox {

    String display();
    void setOrigin(int x, int y);
    void setExtent(int width, int height);

}
