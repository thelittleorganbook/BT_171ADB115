package demo.bachelorthesis.patterns.structural.adapter;

public interface Shape {

    String display();
    void boundingBox();
    void setOrigin(int x, int y);
    void setExtent(int width, int height);

}
