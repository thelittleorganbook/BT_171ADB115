package demo.bachelorthesis.patterns.structural.decorator;

public class TextView extends VisualComponent{

    public TextView() {
        super();
        this.drawingProgress = "Text View";
    }

    @Override
    public String draw() {
        return String.format("%s is being drawn", this.drawingProgress);
    }

}
