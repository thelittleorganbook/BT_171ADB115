package demo.bachelorthesis.patterns.structural.decorator;

import lombok.Getter;

public abstract class VisualComponent {

    @Getter
    protected String drawingProgress;

    public abstract String draw();

}
