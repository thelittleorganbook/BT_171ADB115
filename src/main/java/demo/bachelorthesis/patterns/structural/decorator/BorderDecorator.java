package demo.bachelorthesis.patterns.structural.decorator;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class BorderDecorator extends ComponentDecorator {

    private VisualComponent component;

    @Override
    public String draw() {
        return this.component.draw() + drawBorder();
    }

    private String drawBorder() {
        return " with border";
    }

}
