package demo.bachelorthesis.patterns.structural.decorator;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ScrollDecorator extends ComponentDecorator {

    private VisualComponent component;

    @Override
    public String draw() {
        return this.component.draw() + drawScrollBar();
    }

    private String drawScrollBar() {
        return " with scroll bar";
    }

}
