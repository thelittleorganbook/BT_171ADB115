package demo.bachelorthesis.patterns.structural.proxy;

public class Image implements Graphic {

    private String fileName;

    @Override
    public void store(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public String loadByName(String fileName) {
        return String.format("Image file named '%s' is loaded",
                fileName.equals(this.fileName) ? fileName : null);
    }

}
