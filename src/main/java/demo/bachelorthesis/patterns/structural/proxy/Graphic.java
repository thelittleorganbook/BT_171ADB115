package demo.bachelorthesis.patterns.structural.proxy;

public interface Graphic {

    void store(String fileName);
    String loadByName(String fileName);

}
