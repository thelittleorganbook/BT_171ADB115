package demo.bachelorthesis.patterns.behavioural.observer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Observable;

@Slf4j
@Service
public class CalculationResult extends Observable {

    public CalculationResult(List<Observer> observers) {
        observers.forEach(this::addObserver);
    }

    public void calculateValueA(int b, int c) {
        log.info("Division '{} / {}' is in progress", b, c);
        Integer valueA = b % c;
        update(valueA);
    }

    public void update(Integer valueA){
        this.setChanged();
        this.notifyObservers(valueA);
    }

}
