package demo.bachelorthesis.patterns.behavioural.observer;

import lombok.extern.slf4j.Slf4j;

import java.util.Observable;

@Slf4j
public class BarChartObserver extends Observer {

    private Integer columnA;

    @Override
    public void update(Observable o, Object arg) {
        columnA = (Integer) arg;
        log.info("Bar chart column A updated with the remainder {}", columnA);
    }

}
