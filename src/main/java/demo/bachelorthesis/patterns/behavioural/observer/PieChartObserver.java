package demo.bachelorthesis.patterns.behavioural.observer;

import lombok.extern.slf4j.Slf4j;

import java.util.Observable;

@Slf4j
public class PieChartObserver extends Observer {

    private Integer pieceA;

    @Override
    public void update(Observable o, Object arg) {
        pieceA = (Integer) arg;
        log.info("Pie chart piece A updated with the remainder {}", pieceA);
    }

}
