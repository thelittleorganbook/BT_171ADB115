package demo.bachelorthesis.patterns.behavioural.mediator;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class FontListBox {

    @Setter
    private FontDialogMediator mediator;

    private Font font;

    public void selectionChanged(Font selectedFont) {
        mediator.setFontChanged(!selectedFont.equals(font));
        this.font = selectedFont;
        log.info("List box selected font : {}", font.getValue());
    }

}
