package demo.bachelorthesis.patterns.behavioural.mediator;

import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;

import javax.annotation.PostConstruct;

@RequiredArgsConstructor
@Component
@Setter
@Slf4j
public class FontDialogMediator {

    private final FontListBox listBox;
    private final TextEntryField entryField;

    @PostConstruct
    public void init() {
        listBox.setMediator(this);
        entryField.setMediator(this);
    }

    private boolean textFieldIsEmpty = true;
    private boolean fontChanged;

    public void enterText(String enteredText) {
        entryField.setText(enteredText);
    }

    public void selectFont(Font font) {
        listBox.selectionChanged(font);
        if (!textFieldIsEmpty && fontChanged) {
            entryField.setFont(font);
        } else {
            log.info("Text font not changed");
        }
    }

}
