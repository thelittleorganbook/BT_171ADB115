package demo.bachelorthesis.patterns.behavioural.mediator;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@AllArgsConstructor
public enum Font {

    BOLD("bold"),
    ITALIC("italic"),
    UNDERLINED("underlined");

    @Getter
    private String value;

    public static boolean isValid(String value) {
        return enumOf(value) != null;
    }

    public static Font enumOf(String value) {
        return Arrays.stream(Font.values())
                .filter(v -> v.getValue().equals(value))
                .findFirst()
                .orElse(null);
    }

}
