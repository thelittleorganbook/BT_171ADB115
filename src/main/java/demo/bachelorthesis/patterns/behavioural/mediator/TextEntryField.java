package demo.bachelorthesis.patterns.behavioural.mediator;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class TextEntryField {

    @Setter
    private FontDialogMediator mediator;

    public void setText(String text) {
        mediator.setTextFieldIsEmpty(text.isEmpty());
        log.info("Text field's content : {}", text);
    }

    public void setFont(Font font) {
        log.info("Text field's text font : {}", font);
    }

}
