package demo.bachelorthesis.patterns.behavioural.template;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class View {

    public abstract void doDisplay();

    protected final void setFocus() {
        log.info("Focus set for the current view");
    }

    protected final void resetFocus() {
        log.info("Focus reset for the calling view");
    }

}

