package demo.bachelorthesis.patterns.behavioural.template;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Qualifier("interact")
public class InteractiveView extends View {

    @Override
    public void doDisplay() {
        log.info("Interactive view is displayed");
        this.setFocus();
        addCommands();
        this.resetFocus();
    }

    private void addCommands() {
        log.info("View elements are supplied with default commands");
    }

}
