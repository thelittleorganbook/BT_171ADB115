package demo.bachelorthesis.patterns.behavioural.template;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Qualifier("resize")
public class ResizableView extends View {

    @Override
    public void doDisplay() {
        log.info("Resizable view is displayed");
        this.setFocus();
        addResizeButtons();
        this.resetFocus();
    }

    private void addResizeButtons() {
        log.info("View is supplied with resize functionality");
    }

}
