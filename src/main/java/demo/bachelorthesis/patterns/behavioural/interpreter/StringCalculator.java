package demo.bachelorthesis.patterns.behavioural.interpreter;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;

import java.util.HashMap;
import java.util.Map;

@Getter
public class StringCalculator {

    private final Map<String, Integer> convertInput = new HashMap<>();
    // not thread-safe
    private final String input1;
    private final String input2;

    public StringCalculator(String input1, String input2) {
        initMap();
        this.input1 = input1;
        this.input2 = input2;
    }

    @Value("#{calculator.convertInput[calculator.input1] " +
            "+ calculator.convertInput[calculator.input2]}")
    private Integer sum;

    @Value("#{calculator.convertInput[calculator.input1] " +
            "- calculator.convertInput[calculator.input2]}")
    private Integer subtraction;

    @Value("#{calculator.convertInput[calculator.input1] " +
            "* calculator.convertInput[calculator.input2]}")
    private Integer multiplication;

    @Value("#{calculator.convertInput[calculator.input2] == 0 " +
            "? null " +
            ": calculator.convertInput[calculator.input1] " +
            "/ calculator.convertInput[calculator.input2]}")
    private Integer division;

    private void initMap() {
        convertInput.put("zero", 0);
        convertInput.put("one", 1);
        convertInput.put("two", 2);
        convertInput.put("three", 3);
        convertInput.put("four", 4);
        convertInput.put("five", 5);
        convertInput.put("six", 6);
        convertInput.put("seven", 7);
        convertInput.put("eight", 8);
        convertInput.put("nine", 9);
        convertInput.put("ten", 10);
        convertInput.put("eleven", 11);
        convertInput.put("twelve", 12);
        convertInput.put("thirteen", 13);
        convertInput.put("fourteen", 14);
        convertInput.put("fifteen", 15);
        convertInput.put("sixteen", 16);
        convertInput.put("seventeen", 17);
        convertInput.put("eighteen", 18);
        convertInput.put("nineteen", 19);
        convertInput.put("twenty", 20);
    }


}
