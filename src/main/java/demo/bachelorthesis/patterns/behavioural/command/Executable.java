package demo.bachelorthesis.patterns.behavioural.command;

@FunctionalInterface
public interface Executable {

    void execute(String text);

}
