package demo.bachelorthesis.patterns.behavioural.command;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Getter
@Component
public class Document {

    // not thread-safe
    private String copiedText;
    private String pastedText;

    public void copy(String text) {
        copiedText = text;
        log.info("'{}' is copied.", text);
    }

    public void paste() {
        pastedText = copiedText;
        log.info("'{}' is pasted.", copiedText);
    }

}
