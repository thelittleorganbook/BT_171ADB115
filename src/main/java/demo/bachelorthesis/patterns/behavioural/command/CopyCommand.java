package demo.bachelorthesis.patterns.behavioural.command;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class CopyCommand implements Executable {

    private final Document document;

    @Override
    public void execute(String text) {
        document.copy(text);
    }

}
