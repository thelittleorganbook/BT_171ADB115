package demo.bachelorthesis.patterns.creational.builder;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class CustomerOccupation {

    private String lastName;
    private String firstName;
    private boolean employed;
    private String company;
    private String occupation;

}
