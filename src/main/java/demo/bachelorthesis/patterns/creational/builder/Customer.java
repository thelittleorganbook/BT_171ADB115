package demo.bachelorthesis.patterns.creational.builder;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

@Getter
@Builder
public class Customer {

    @NonNull
    private final String lastName;
    @NonNull
    private final String firstName;
    private final boolean employed;
    private final String company;
    private final String occupation;
    private final String middleName;
    private final String salutation;
    private final String suffix;
    private final String streetAddress;
    private final String city;
    private final String state;
    private final boolean female;
    private final boolean homeOwner;

}
