package demo.bachelorthesis.patterns.creational.factory;

import org.springframework.stereotype.Component;

@Component
public class ControlElementsFactory {

    private static final String RADIO_BUTTON = "radiobutton";
    private static final String LABEL = "label";

    public ControlElement createElement(String type) {
        switch (type.toLowerCase()) {
            case RADIO_BUTTON: return new RadioButton();
            case LABEL: return new Label();
            default: throw new UnsupportedOperationException("Factory does not produce such control element");
        }
    }

}
