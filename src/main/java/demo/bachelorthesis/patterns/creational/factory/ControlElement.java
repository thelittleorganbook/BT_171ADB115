package demo.bachelorthesis.patterns.creational.factory;

public interface ControlElement {

    void makeVisible(int opacity);
    int getOpacity();
    boolean isVisible();

}


