package demo.bachelorthesis.patterns.creational.factory;

import lombok.Getter;

@Getter
public class RadioButton implements ControlElement {

    private String type = "radiobutton";
    private int opacity;
    private boolean isVisible;

    @Override
    public void makeVisible(int opacity) {
        this.opacity = opacity;
        this.isVisible = true;
    }

}
