package demo.bachelorthesis.patterns.creational.singleton;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TraditionalSingleton {

    private static TraditionalSingleton instance;

    public static TraditionalSingleton getInstance() {
        if (null == instance) {
            synchronized (TraditionalSingleton.class) {
                if (null == instance) {
                    instance = new TraditionalSingleton();
                }
            }
        }
        return instance;
    }

}
