package demo.bachelorthesis.patterns.other.repository;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@SuppressWarnings("ALL")
@Table(name= "CUSTOMER")
@Getter
@Setter
@Entity
public class CustomerEntity {

    @Id
    @GeneratedValue
    @Column(name="CUSTOMER_ID")
    private long id;
    @Column(name="FIRST_NAME")
    private String firstName;
    @Column(name="MIDDLE_INITIAL")
    private String middleInitial;
    @Column(name="LAST_NAME")
    private String lastName;
    @Column(name="EMAIL_ADDRESS")
    private String emailAddress;

}
